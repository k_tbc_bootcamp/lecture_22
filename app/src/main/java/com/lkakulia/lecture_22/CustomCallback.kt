package com.lkakulia.lecture_22

interface CustomCallback {
    fun onFailure(error: String?)
    fun onResponse(response: String)
}