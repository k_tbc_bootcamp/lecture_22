package com.lkakulia.lecture_22

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.recyclerview_layout.view.*

class RecyclerViewAdapter(private val users: MutableList<UserModel>,
                          private val customListener: CustomListener): RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(
            R.layout.recyclerview_layout,
            parent,
            false
        ))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int {
        return users.size
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private lateinit var user: UserModel

        fun onBind() {
            user = users[adapterPosition]

            itemView.modifyUserButton.setOnClickListener {
                customListener.onClick(adapterPosition)
            }

            itemView.nameTextView.text = user.name
            itemView.jobTextView.text = user.job
            itemView.idTextView.text = user.id
            itemView.createdAtTextView.text = user.createdAt
            itemView.updatedAtTextView.text = user.updatedAt
        }
    }
}