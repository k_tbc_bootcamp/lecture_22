package com.lkakulia.lecture_22

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_modify_user.*

class ModifyUserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_modify_user)

        init()
    }

    private fun init() {
        val name = intent.getStringExtra("name")
        val job = intent.getStringExtra("job")
        val id = intent.getStringExtra("id")
        nameEditText.setText(name)
        nameEditText.isEnabled = false
        jobEditText.setText(job)

        modifyUserButton.setOnClickListener {
            val job = jobEditText.text.toString()
            val parameters = mutableMapOf<String, String>()
            parameters["name"] = name!!
            parameters["job"] = job


            if (job.isNotEmpty()) {
                HttpRequest.putRequest(HttpRequest.USERS, id!!, parameters, object: CustomCallback {
                    override fun onFailure(error: String?) {
                        Toast.makeText(this@ModifyUserActivity, error, Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(response: String) {
                        val intent = Intent(this@ModifyUserActivity, UsersDashboardActivity::class.java).apply {
                            putExtra("response", response)
                        }
                        setResult(Activity.RESULT_OK, intent)
                        finish()
                    }

                })
            }
            else {
                Toast.makeText(this@ModifyUserActivity, "Please fill in the job field", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
