package com.lkakulia.lecture_22

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_users_dashboard.*
import kotlinx.android.synthetic.main.recyclerview_layout.*
import org.json.JSONObject

class UsersDashboardActivity : AppCompatActivity() {

    private val users = mutableListOf<UserModel>()
    private lateinit var adapter: RecyclerViewAdapter
    private var modifyPosition = 0

    companion object {
        const val UPDATE_USER_REQUEST_CODE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_users_dashboard)

        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(users, object: CustomListener {
            override fun onClick(adapterPosition: Int) {
                modifyPosition = adapterPosition
                val user = users[adapterPosition]

                val intent = Intent(this@UsersDashboardActivity, ModifyUserActivity::class.java). apply {
                    putExtra("name", user.name)
                    putExtra("job", user.job)
                    putExtra("id", user.id)
                }
                startActivityForResult(intent, UPDATE_USER_REQUEST_CODE)
            }

        })
        recyclerView.adapter = adapter
        init()
    }

    private fun init() {
        createUserButton.setOnClickListener {
            val name = nameEditText.text.toString()
            val job = jobEditText.text.toString()

            if (name.isNotEmpty() && job.isNotEmpty()) {
                createUserButton.visibility = View.GONE
                val parameters = mutableMapOf<String, String>()
                parameters["name"] = name
                parameters["job"] = job
                users.add(0, UserModel(name, job, "", "", "NOT YET UPDATED"))

                HttpRequest.postRequest(HttpRequest.USERS, parameters, object: CustomCallback {
                    override fun onFailure(error: String?) {
                        createUserButton.visibility = View.VISIBLE
                        Toast.makeText(this@UsersDashboardActivity, error, Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(response: String) {
                        createUserButton.visibility = View.VISIBLE
                        val json = JSONObject(response)

                        if (json.has("id"))
                            users[0].id = json.getString("id")

                        if (json.has("createdAt"))
                            users[0].createdAt = json.getString("createdAt")

                        adapter.notifyItemInserted(0)
                        recyclerView.scrollToPosition(0)
                        nameEditText.text.clear()
                        jobEditText.text.clear()
                    }
                })
            }
            else {
                Toast.makeText(this, "Please fill in all the fields", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == UPDATE_USER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val response = data!!.getStringExtra("response")!!
            val json = JSONObject(data!!.getStringExtra("response")!!)

            if (json.has("job"))
                users[modifyPosition].job = json.getString("job")

            if (json.has("updatedAt"))
                users[modifyPosition].updatedAt = json.getString("updatedAt")

            adapter.notifyItemChanged(modifyPosition)
        }
    }

}
