package com.lkakulia.lecture_22

interface CustomListener {
    fun onClick(adapterPosition: Int)
}